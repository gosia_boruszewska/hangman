package com.example.mborusze.hangman;

import java.util.ArrayList;

public class CheckWord {
    public static boolean checkWord(String wordToCheck, String word) {
        return  word.equals(wordToCheck);
    }
}
