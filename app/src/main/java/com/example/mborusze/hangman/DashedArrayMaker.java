package com.example.mborusze.hangman;


import java.util.ArrayList;

public class DashedArrayMaker {
    public static ArrayList<Character> makeDashedArray(ArrayList<Character> word) {

        for (int i = 0 ; i < word.size();i++
                ) {
            word.set(i,'_');
        }
        return word;
    }
}
