package com.example.mborusze.hangman;

public class LetterNoLetterChecker {
    private String letterNoLetter;

    public LetterNoLetterChecker(String letterNoLetter){
        this.letterNoLetter = letterNoLetter;
    }

    public boolean isNoLetter(){
        return !(letterNoLetter.length()==1);
    }

    public boolean isEmptyString(){
        return letterNoLetter.length()==0;
    }
}
