package com.example.mborusze.hangman;

import android.content.DialogInterface;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.api.GoogleApiClient;

import java.util.ArrayList;
import java.util.Random;
public class MainActivity extends AppCompatActivity {
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private static final String HANGMAN = "hangman";
    private GoogleApiClient client;

    private WordGenerator wordGenerator;
    private GeneratedWordManager generatedWordManager;
    private ImageViewManager imageViewManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();

        imageViewManager = new ImageViewManager();
        imageViewManager.setImageViewArrayList(getImageViewList());

        wordGenerator = new WordGenerator(getResources());

        generatedWordManager = new GeneratedWordManager();

    }

    private ArrayList<ImageView> getImageViewList(){
        ArrayList<ImageView> imageViewList;
        imageViewList = new ArrayList<ImageView>();
        return setImageViewList(imageViewList);
    }

    private ArrayList<ImageView> setImageViewList(ArrayList<ImageView> imageViewList){
        imageViewList.add((ImageView) findViewById(R.id.hangman0));
        imageViewList.add((ImageView) findViewById(R.id.hangman1));
        imageViewList.add((ImageView) findViewById(R.id.hangman2));
        imageViewList.add((ImageView) findViewById(R.id.hangman3));
        imageViewList.add((ImageView) findViewById(R.id.hangman4));
        imageViewList.add((ImageView) findViewById(R.id.hangman5));
        imageViewList.add((ImageView) findViewById(R.id.hangman6));
        imageViewList.add((ImageView) findViewById(R.id.hangman7));
        imageViewList.add((ImageView) findViewById(R.id.hangman8));
        imageViewList.add((ImageView) findViewById(R.id.hangman9));
        imageViewList.add((ImageView) findViewById(R.id.hangman10));
        imageViewList.add((ImageView) findViewById(R.id.hangman_gray));
        return imageViewList;
    }

    public void checkLetterOnClick(View view) {

        String letterToCheck = getStringFromInput();
        LetterNoLetterChecker letterNoLetterChecker = new LetterNoLetterChecker(letterToCheck);

        if(letterNoLetterChecker.isEmptyString()){
            createPutSomeLetterDialog();
            return;
        }
        if(letterNoLetterChecker.isNoLetter()){
            createCheckWordDialog();
            return;
        }

        if (CheckLetter.checkLetter(letterToCheck.charAt(0), generatedWordManager.getGeneratedWordString())) {
            generatedWordManager.setGeneratedWordArrayList(letterToCheck.charAt(0));
            putLegalWordToOutput(ReadableOutputMaker.makeLegalWordToOutput(generatedWordManager.getGeneratedWordArrayList()));
            clearText();
            if(generatedWordManager.isNoUnderscores())
                createWinDialog();
        } else {
            imageViewManager.displayNextImage();
            clearText();
            if(imageViewManager.isThisImageTheLastOne())
                createLoseDialog();
        }
    }

    private String getStringFromInput(){
        return ((TextView)findViewById(R.id.letterWordInput))
                .getText()
                .toString()
                .toUpperCase();
    }

    private void putLegalWordToOutput(String output){
        ((TextView)findViewById(R.id.mysteryWord)).setText(output);
    }

    private void clearText(){
        ((TextView)findViewById(R.id.letterWordInput)).setText("");
    }

    private void createLoseDialog(){
        new AlertDialog.Builder(this)
                .setTitle(getResources().getString(R.string.dead_hangman_title))
                .setMessage(getResources().getString(R.string.dead_hangman_content))
                .setCancelable(false)
                .setIcon(R.drawable.dead_hangman)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        createNewGameDialog();
                    }
                }).show();
    }

    private void createCheckWordDialog(){
        new AlertDialog.Builder(this)
                .setTitle(getResources().getString(R.string.check_word_title))
                .setMessage(getResources().getString(R.string.check_word_content))
                .setCancelable(false)
                .setIcon(R.drawable.info_hangman)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                }).show();
    }

    private void createPutSomeLetterDialog(){
        new AlertDialog.Builder(this)
                .setTitle(getResources().getString(R.string.no_letter_or_word_title))
                .setMessage(getResources().getString(R.string.no_letter_or_word_content))
                .setCancelable(false)
                .setIcon(R.drawable.info_hangman)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                }).show();
    }

    public void generateWordOnClick(View view) {
        visibleAfterWordGeneration();
        generatedWordManager.setGeneratedWordString(wordGenerator.generateWord());

        Log.d("generates WORD",generatedWordManager.getGeneratedWordString());

        putLegalWordToOutput(ReadableOutputMaker.makeLegalWordToOutput(generatedWordManager.getGeneratedWordArrayList()));
        imageViewManager.setFirstImage();
    }

    private void visibleAfterWordGeneration(){
        (findViewById(R.id.letterVerification)).setEnabled(true);
        (findViewById(R.id.wordVerification)).setEnabled(true);
        (findViewById(R.id.mysteryWord)).setVisibility(View.VISIBLE);
    }

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    public Action getIndexApiAction() {
        Thing object = new Thing.Builder()
                .setName("Main Page") // TODO: Define a title for the content shown.
                // TODO: Make sure this auto-generated URL is correct.
                .setUrl(Uri.parse("http://[ENTER-YOUR-URL-HERE]"))
                .build();
        return new Action.Builder(Action.TYPE_VIEW)
                .setObject(object)
                .setActionStatus(Action.STATUS_TYPE_COMPLETED)
                .build();
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        AppIndex.AppIndexApi.start(client, getIndexApiAction());
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        AppIndex.AppIndexApi.end(client, getIndexApiAction());
        client.disconnect();
    }

    public void checkWordOnClick(View view) {
        String wordToCheck = getStringFromInput();
        LetterNoLetterChecker letterNoLetterChecker = new LetterNoLetterChecker(wordToCheck);

        if(letterNoLetterChecker.isEmptyString()){
            createPutSomeLetterDialog();
            return;
        }

        boolean isWordCorrect = CheckWord.checkWord(wordToCheck,generatedWordManager.getGeneratedWordString());

        if(isWordCorrect) {
            createWinDialog();
        }
        else {
            imageViewManager.displayNextImage();
            clearText();
        }
    }

    private void createWinDialog(){
        new AlertDialog.Builder(this)
                .setTitle(getResources().getString(R.string.win_title))
                .setMessage(getResources().getString(R.string.win_content))
                .setCancelable(false)
                .setIcon(R.drawable.superman)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        createNewGameDialog();
                    }
                }).show();
    }

    private void createNewGameDialog() {
        new AlertDialog.Builder(this)
                .setTitle(getResources().getString(R.string.new_game_title))
                .setMessage(getResources().getString(R.string.new_game_content))
                .setCancelable(false)
                .setIcon(R.drawable.new_hangman)
                .setPositiveButton(getResources().getString(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        startNewGame();
                    }
                })
                .setNegativeButton(getResources().getString(R.string.no), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        System.exit(0);
                    }
                }).show();
    }



    private void startNewGame(){
        cleanForStart();
        invisibleForStart();
        imageViewManager.setFirstImage();
    }

    private void cleanForStart(){
        ((TextView)findViewById(R.id.letterWordInput)).setText("");
        ((TextView)findViewById(R.id.mysteryWord)).setText(null);
        ((TextView)findViewById(R.id.mysteryWord)).setVisibility(View.INVISIBLE);
    }

    private void invisibleForStart(){
        (findViewById(R.id.letterVerification)).setEnabled(false);
        (findViewById(R.id.wordVerification)).setEnabled(false);
        (findViewById(R.id.mysteryWord)).setVisibility(View.INVISIBLE);
    }
}