package com.example.mborusze.hangman;

import android.content.res.Resources;

import java.util.Random;

public class WordGenerator {

    private Resources resources;

    public WordGenerator(Resources resources){
        this.resources = resources;
    }

    public String generateWord(){
        String [] wordsToGenerate = resources.getStringArray(R.array.words);
        int index  = new Random().nextInt(wordsToGenerate.length);
        return wordsToGenerate[index];
    }

}
