package com.example.mborusze.hangman;

import android.view.View;
import android.widget.ImageView;

import java.util.ArrayList;

public class ImageViewManager {

    private ArrayList<ImageView> imageViewArrayList;
    private int indexImageViewArrayList;

    public ImageViewManager(){
        indexImageViewArrayList = 0;
    }

    public void displayNextImage(){
        makeImageInvisible();
        incrementIndex();
        makeVisibleImage();
    }

    private void makeImageInvisible(){
        imageViewArrayList.get(indexImageViewArrayList).setVisibility(View.INVISIBLE);
    }

    private void makeVisibleImage(){
        imageViewArrayList.get(indexImageViewArrayList).setVisibility(View.VISIBLE);
    }

    private void incrementIndex(){
        indexImageViewArrayList++;
    }

    private void zeroIndex(){
        indexImageViewArrayList = 0;
    }

    public boolean isThisImageTheLastOne(){
        return indexImageViewArrayList==11;
    }

    public void setFirstImage() {
        invisibleAllImages();
        zeroIndex();
        makeVisibleImage();
    }

    private void invisibleAllImages(){
        for (ImageView i:imageViewArrayList
                ) {
            i.setVisibility(View.INVISIBLE);
        }
    }

    public void setImageViewArrayList(ArrayList<ImageView> imageViewArrayList) {
        this.imageViewArrayList = imageViewArrayList;
    }

}
