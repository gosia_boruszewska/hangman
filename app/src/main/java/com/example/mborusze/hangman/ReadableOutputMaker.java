package com.example.mborusze.hangman;


import java.util.ArrayList;

public class ReadableOutputMaker {
    public static String makeLegalWordToOutput(ArrayList<Character> currentOutput){
        String legalOutput = currentOutput.toString();
        legalOutput = legalOutput.replaceAll("[^a-zA-Z_]", "");
        legalOutput = legalOutput.replace("", " ").trim();
        return legalOutput;
    }
}
