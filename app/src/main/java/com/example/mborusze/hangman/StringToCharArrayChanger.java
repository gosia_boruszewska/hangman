package com.example.mborusze.hangman;


import java.util.ArrayList;

public class StringToCharArrayChanger {
    public static ArrayList<Character> changeToArray(String word){
        ArrayList<Character> wordCharactersList = new ArrayList<>();
        for (char letter:word.toCharArray()
                ) {
            wordCharactersList.add(letter);
        }
        return wordCharactersList;
    }
}
