package com.example.mborusze.hangman;


import android.widget.ArrayAdapter;

import java.util.ArrayList;

public class GeneratedWordManager {
    private String generatedWordString;
    private ArrayList<Character> generatedWordArrayList;

    public String getGeneratedWordString(){
        return this.generatedWordString;
    }

    public void setGeneratedWordString(String generatedWordString) {
        this.generatedWordString = generatedWordString.toUpperCase();
        changeGeneratedWordToArray();
        makeGeneratedWordDashed();
    }

    private void changeGeneratedWordToArray(){
        this.generatedWordArrayList = StringToCharArrayChanger.changeToArray(generatedWordString);
    }

    private void makeGeneratedWordDashed(){
        this.generatedWordArrayList = DashedArrayMaker.makeDashedArray(generatedWordArrayList);
    }

    public void setGeneratedWordArrayList(Character letter){
        generatedWordChanger(letter);
    }

    private void generatedWordChanger(Character letter){
        for (int i = 0; i < generatedWordString.length(); i++ ) {
            if (letter==generatedWordString.charAt(i))
                generatedWordArrayList.set(i,letter);
        }
    }

    public ArrayList<Character> getGeneratedWordArrayList(){
        return  this.generatedWordArrayList;
    }

    public boolean isGivenWordCorrect(String word){
        word = word.toUpperCase();
        return word
                .equals(generatedWordString);
    }

    public boolean isNoUnderscores(){
        return !(generatedWordArrayList.contains('_'));
    }
}
