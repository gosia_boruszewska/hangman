package com.example.mborusze.hangman;

import java.util.ArrayList;

public class CheckLetter {

    public static boolean checkLetter(char letter, String word) {
        ArrayList<Character> arrayList = StringToCharArrayChanger.changeToArray(word);
        return  arrayList.contains(letter);
        
    }
}
